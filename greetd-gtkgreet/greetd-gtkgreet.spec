Name:           greetd-gtkgreet
Version:        0.7
Release:        0.1%{?dist} 
Summary:        GTK based greeter for greetd
License:        GPLv3
URL:            https://git.sr.ht/~kennylevinsen/gtkgreet

Source0:        https://git.sr.ht/~kennylevinsen/gtkgreet/archive/0.7.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  ninja-build
BuildRequires:  pkgconfig(gtk-layer-shell-0)
BuildRequires:  scdoc
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(json-c)

%description
GTK based greeter for greetd, to be run under cage or similar.

%changelog
* Mon Feb 7 2022 KJ Tsanaktsidis <kj@kjtsanaktsidis.id.au> - 0.7-1
- Initial release of the greetd-gtkgreet package

%prep
%autosetup -n gtkgreet-%{version_no_tilde} -p1

%build
%meson
%meson_build
scdoc < man/gtkgreet.1.scd > man/gtkgreet.1

%install
%meson_install
install -p -D -m 644 LICENSE %{buildroot}%{_docdir}/%{name}-%{version}/LICENSE
install -p -D -m 644 man/gtkgreet.1 %{buildroot}%{_mandir}/man1/gtkgreet.1

%files
%defattr(-,root,root)
%attr(755, root, root) %{_bindir}/gtkgreet
%license %{_docdir}/%{name}-%{version}/LICENSE
%doc %{_mandir}/man1/gtkgreet.1*


