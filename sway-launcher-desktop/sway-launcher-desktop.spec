Name:           sway-launcher-desktop
Version:        1.5.4
Release:        1%{?dist}
Summary:        TUI Application launcher with Desktop Entry support
License:        GPLv3
URL:            https://github.com/Biont/sway-launcher-desktop
Source0:        https://github.com/Biont/sway-launcher-desktop/archive/refs/tags/v%{version}.tar.gz
Requires:       fzf

%description
TUI Application launcher with Desktop Entry support. Made for SwayWM, but runs anywhere.

%prep
%setup -q

%install
rm -rf %{buildroot}
install -p -D -m 755 sway-launcher-desktop.sh %{buildroot}%{_bindir}/sway-launcher-desktop
install -p -D -m 644 LICENSE %{buildroot}%{_docdir}/%{name}-%{version}/LICENSE
 
%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(755, root, root) %{_bindir}/sway-launcher-desktop
%license %{_docdir}/%{name}-%{version}/LICENSE

%changelog
* Mon Feb 7 2022 KJ Tsanaktsidis <kj@kjtsanaktsidis.id.au> - 1.5.4-1
- Initial release of the sway-launcher-desktop package
